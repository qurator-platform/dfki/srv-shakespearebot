#!/usr/bin/env python
import json

# https://pypi.org/project/pika/
import pika

# https://pypi.org/project/PyYAML/
import yaml

import chatspeare
import logging
logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# get host, port, username, password from /etc/elg/mq.yaml
# (but assume localhost if file not found)
# See https://gitlab.com/european-language-grid/platform/elg-apis/blob/master/doc/T2.5-api-design.md#container-configuration
try:
    with open("/etc/elg/mq.yaml", "r") as stream:
        try:
            config = yaml.safe_load(stream)
        except yaml.YAMLError as e:
            exit()
        # initialise password etc
        connection_parameters = {
            "host": config["host"],
            "port": config["port"],
            "credentials": pika.PlainCredentials(config["username"], config["password"])
                #{
            #"username": config["username"],
            #"password": config["password"],
            #}
        }
        #credentials = pika.PlainCredentials(config["username"], config["password"])
        queue_name = config["topic_in"]
except FileNotFoundError:
    # Assume running on laptop (not in ELG grid),
    # so assume RabbitMQ server on localhost
    connection_parameters = {
        "host": "localhost"
    }
    queue_name = "my-service-in"

connection = pika.BlockingConnection(pika.ConnectionParameters(**connection_parameters))

channel = connection.channel()

# queue name is topic_in in mq.yaml
channel.queue_declare(queue=queue_name, durable=True)
# channel.exchange_declare(exchange="rpc_queue")
# channel.queue_bind(....)


# init and train bot
chatbot = chatspeare.init("Eddie", logger=logger)
#chatspeare.train(chatbot)


def on_request(ch, method, props, body):
    print("   [x] receiving message")
    request = json.loads(body)
    text_to_process = request["request"]["content"]

    # do work here
    response_string = chatspeare.response(chatbot, text_to_process)
    print("Responding with ", response_string)

    # construct response message as a dict
    response = {
        "type": "texts",
        "texts": [
            {
                "text": response_string,
            }
        ]
    }

    response_message = {
        "metadata": request["metadata"],
        "response": response
    }

    ch.basic_publish(
        exchange="",
        routing_key=props.reply_to,
        properties=pika.BasicProperties(
            correlation_id=props.correlation_id, content_type="application/json"
        ),
        body=json.dumps(response_message),
    )
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue=queue_name, on_message_callback=on_request)

print(" [x] Awaiting RPC requests", connection_parameters)
channel.start_consuming()
