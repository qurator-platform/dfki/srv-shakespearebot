# author: Ela Elsholz
# date: July 2019
# description: chitter-chatter chatbot responds with 'most similar' Shakespeare quotes from Hamlet
#                      (similar according to cosine similarity)
# realized using python chatbot framework chatterbot:
# https://chatterbot.readthedocs.io/en/stable/, accessed 18.08.2018

import cherrypy
from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
import pandas as pd



class MyWebService(object):

    @cherrypy.expose
    @cherrypy.tools.json_out()
    @cherrypy.tools.json_in()
    def index(self, input):

        # initialize chatbot
        chatbot = ChatBot("Eddie")

        # train chatbot
        corpus = pd.read_csv('corpora/hamlet.csv', sep='\t')
        trainer = ListTrainer(chatbot)
        trainer.train(corpus["original"])


        # calculate response
        output = {"result": chatbot.get_response(input).text }

        return output

    index.exposed = True


if __name__ == '__main__':
    config = {'server.socket_host': '0.0.0.0'}
    cherrypy.config.update(config)
    cherrypy.quickstart(MyWebService())
