from flask import Flask
from flask import request

# creates a Flask application, named app
app = Flask(__name__)

import chatspeare

# init and train bot
chatbot = chatspeare.init("Eddie")
#chatspeare.train(chatbot)

@app.route("/", methods=['GET'])
def display_bot_message():
    user_input = request.args.get('msg')
    if user_input is not None:
        return chatspeare.response(chatbot, user_input)
    return chatspeare.welcome_msg()

# run the application
if __name__ == "__main__":
    app.run(debug=True)