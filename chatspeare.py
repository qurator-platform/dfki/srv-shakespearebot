from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
import pandas as pd

def init(botname):
    '''
    initializes the chatbot
    '''
    return ChatBot(botname)

def train(chatbot):
    '''
    Trains the chatbot using a corpus file
    '''
    corpus = pd.read_csv('corpora/hamlet.csv', sep='\t')
    trainer = ListTrainer(chatbot)
    trainer.train(corpus["original"])
    return chatbot


def response(chatbot, request):
    '''
    Takes a chatbot obtained from the train function and a request string;
    returns an appropriate quote from the corpus.
    '''
    return chatbot.get_response(request).text

def welcome_msg():
    '''

    '''
    msg = "Speak. I am bound to hear."
    return msg
