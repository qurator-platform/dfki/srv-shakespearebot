from python:3

COPY chatspeare.py .
COPY flask-api.py .
#COPY elg-mq.py .
#COPY corpora/hamlet.csv corpora/
COPY db.sqlite3 .

RUN pip install pandas
RUN pip install flask
#RUN pip install pika
#RUN pip install PyYAML

RUN pip install chatterbot
RUN pip install chatterbot-corpus


EXPOSE 8080
ENTRYPOINT FLASK_APP=flask-api.py flask run --host=0.0.0.0 --port=8080
#ENTRYPOINT ["python","elg-mq.py"]
