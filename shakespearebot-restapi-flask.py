from flask import Flask
from flask import request
app = Flask(__name__)

from chatterbot import ChatBot
#from chatterbot.trainers import ListTrainer
#import pandas as pd

# request.values https://stackoverflow.com/questions/10434599/how-to-get-data-received-in-flask-request
# https://scotch.io/bar-talk/processing-incoming-request-data-in-flask
@app.route("/", methods=['GET', 'POST'])
def hello():
    if request.method == 'POST':  # this block is only entered when the form is submitted
        question = request.form['question']
        # initialize chatbot
        chatbot = ChatBot("Eddie")

        # train chatbot
        #corpus = pd.read_csv('corpora/hamlet.csv', sep='\t')
        #trainer = ListTrainer(chatbot)
        #trainer.train(corpus["original"])

        # calculate response
        response = chatbot.get_response(question).text
        return '''<h1>Your question: {}</h1>
                  <h1>My answer: {}</h1>
                  <h1>New question?</h1>
                  <form method="POST">
                Your question: <input type="text" name="question"><br>
               <input type="submit" value="Submit"><br>
          </form>'''.format(question, response)

    #return '''<form method="POST">
    #            Your question: <input type="text" name="question"><br>
    #           <input type="submit" value="Submit"><br>
    #      </form>'''

    return '''<!doctype html>
<html>
<head>
  <title>Talk to Shakespeare</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- <link rel="stylesheet" href="bot/style/shakespeare_bot.css"> -->

    <style>
        #img_shakespeare {
  max-height: 100%;
  max-width: 100%;
}

/* headline a bit smaller, to make full dialog window fit on the screen also for normal laptops */
.jumbotron {
    padding: 0.5em 0.6em;
}

#btn_finish {
  margin-bottom: 1em;
}

/* chat box: messages scrolling, input field at the same place */
#dialogBox {
  width: 100%;
  height: 434px;
  border: 2px solid #eee;
  margin-bottom: 20px;
}

#dialogMessageField {
  width: 100%;
  height: 374px;
  padding: 10px 30px;
  overflow-y: scroll;
  border-bottom: 1px solid #eee;
}

#dialogUserInput {
  height: 58px;
  background: white;
  padding: 10px 10px 6px 10px;
  border-bottom: 2px solid #eee;
  cursor: pointer;
}

/* speech bubble source: https://leaverou.github.io/bubbly/ */
.speech-bubble { position: relative;
                 background: #00aabb;
                 border-radius: .4em;
                 color: white;
                 padding: 0.5em 1em;
                 margin-bottom: 0.5em;
}
.speech-bubble:after { content: '';
                       position: absolute;
                       left: 0;
                       top: 50%;
                       width: 0;
                       height: 0;
                       border: 20px solid transparent;
                       border-right-color: #00aabb;
                       border-left: 0;
                       border-bottom: 0;
                       margin-top: -10px;
                       margin-left: -20px;
}

.speech-bubble-user { position: relative;
                      background: #ff0080;
                      border-radius: .4em;
                      color: white;
                      padding: 0.5em 1em;
                      margin-bottom: 0.5em;
}
.speech-bubble-user:after { content: '';
                            position: absolute;
                            right: 0;
                            top: 50%;
                            width: 0;
                            height: 0;
                            border: 20px solid transparent;
                            border-left-color: #ff0080;
                            border-right: 0;
                            border-bottom: 0;
                            margin-top: -10px;
                            margin-right: -20px;
}

    </style>
</head>

<body>
  <div class="jumbotron text-center">
    <h1>ELG Service: Talk to Shakespeare</h1>
  </div>

  <div class="container">
    <div id="content">
    <div class="row">

      <div class="col-sm-12 col-md-6 col-lg-6 ">
        <img id="img_shakespeare" src="static/shakespeare.jpg">
      </div>
      <div class="col-sm-12 col-md-6 col-lg-6">
        <div id="dialogBox">
          <div id="dialogMessageField">
            <div class="speech-bubble">Me Shakespeare, as servant to thee. Tell me thy concern. What sorrow drives your
              heart this minute? Good fellow, speak out.</div>
          </div>
          <div id="scrollHelper">
          </div>
          <form id="dialogUserInput" method="POST">
            <div class="input-group">
              <input type="text" class="form-control" id="question" name="question" autofocus="autofocus"></input>
              <span class="input-group-btn">
                <input type="submit" class="btn btn-default" value="Send"></input>
              </span>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  </div>


      </body>
</html>

    '''
